#!/usr/bin/python3
# https://www.youtube.com/watch?v=X2vAabgKiuM
# https://www.youtube.com/watch?v=imPpT2Qo2sk&list=PLQVvvaa0QuDf2JswnfiGkliBInZnIC4HL&index=5 
import os
import sys
import re 
import tkinter
import nltk
import inspect
import nltk.corpus
from nltk.tokenize import word_tokenize
from nltk import pos_tag
from nltk import RegexpParser
from nltk.corpus import stopwords
import mysql.connector 

def getDatafromDatabase(command):
    # connect to database
    connection = mysql.connector.connect(
        host = 'localhost',
        user = 'IRobot',
        password = 'toor',
        database = 'aidb'
    )

    myCursor = connection.cursor()
    myCursor.execute(command)

    data = myCursor.fetchall()
    connection.close()
    return data


key = """
CC	coordinating conjunction
CD	cardinal digit
DT	determiner
EX	existential there (like: "there is" ... think of it like "there exists")
FW	foreign word
IN	preposition/subordinating conjunction
JJ	adjective	'big'
JJR	adjective, comparative	'bigger'
JJS	adjective, superlative	'biggest'
LS	list marker	1)
MD	modal	could, will
NN	noun, singular 'desk'
NNS	noun plural	'desks'
NNP	proper noun, singular	'Harrison'
NNPS	proper noun, plural	'Americans'
PDT	predeterminer	'all the kids'
POS	possessive ending	parent\'s
PRP	personal pronoun	I, he, she
PRP$	possessive pronoun	my, his, hers
RB	adverb	very, silently,
RBR	adverb, comparative	better
RBS	adverb, superlative	best
RP	particle	give up
TO	to	go 'to' the store.
UH	interjection	errrrrrrrm
VB	verb, base form	take
VBD	verb, past tense	took
VBG	verb, gerund/present participle	taking
VBN	verb, past participle	taken
VBP	verb, sing. present, non-3d	take
VBZ	verb, 3rd person sing. present	takes
WDT	wh-determiner	which
VBPWP	wh-pronoun	who, what
WP$	possessive wh-pronoun	whose
WRB	wh-abverb	where, when
"""
print(key)


#sent = 'give me all records where the Manufacturer is not equal to BMW' #
sent_arr = [
  #'get',
  'get all of the records',
  'give me 10 records where the Manufacturer is BMW',
  'give me all records where the Manufacturer is BMW',
  'give me all records where the Manufacturer is BMW or the Manufacturer is Ferrari',
  'give me all records where the Completion_Rate is greater than 99',
  'give me all records where the Recall_Campaign is equal to 15V312',
  'give me all records where the Recall_Campaign equals 15V312',
  'give me all records where the Manufacturer is not BMW',
  'give me all records where the Manufacturer is not equal to BMW',
  'give me all records where the Manufacturer isn\'t BMW',
  'give me all records where the Manufacturer is BMW or Ferrari',
  'give me all records where the Manufacturer is BMW or Ferrari and Ford',
  'give me the Recall_Campaign , Priority_Group , Driver_Passenger, Stolen, Net_Air_Bags_Affected where the Manufacturer is BMW' 
]

# main chunking function that takes the generic sentence and parses it to 
# individual chunks. Uses Chinking to ensure that certain parts are ommited from specific chunks
def chunker(sent):
    new_tokens = nltk.pos_tag(word_tokenize(sent))
    #print (new_tokens)
    query = []
    # Iterate through chunks using the chunker to classify different parts of speach that could be classified as SQL
    # For each of these use a regex on the label of the chunk to see if we had a hit. 
    # Append this to a list and use this t create the sql query based on conditions
    chunks = [
        r"""SELECTaLL: {<VB|VBP>+<PRP.?>*<DT>+}""",
        r"""SELECTsOME: {<VB|VBP>+<PRP|PRP$>?<CD>+}""",

        r"""COLUMN1: {<NNP|VBN>+<,>+}
                     }<,>+{""",
        r"""COLUMN2: {<,>+<NNP|VBN>+<WRB>+}
                     }<,>+{
                     }<WRB>+{""",
        r"""REC: {<.*>+<NN|NNS>+$}""",
        r"""TABLE:{<NNS>+<IN>+<NNP>+}""",
        r"""COND1: {<WRB|CC>*<DT>*<NN.?>+<VB.?|NN.?>+<NN.?|JJ.?|RB>*<IN|TO>*<NNP|CD|VB.?>+}
              }<WRB>+<DT>+{""" ,
        r"""CONDn: {<CC>+<RB>*<NNP|CD|VBP>+}""" ,
        r"""CONDN: {<CC>+<RB>*<VB.?|NN.?|JJ.?>+<IN>*<NNP|CD|VBP>+}""" ,
        r"""COND@: {<CC>+<RB>*<DT>*<NN.?>+<VB.?|JJ.?>*<NNP|CD|VB.?>+}""" ,
    ]
    # The regex test list is used to determine what chunks we have chunked correctly
    # from the se of chunks
    test = [r'\(SELECTaLL.*',r'\(COLUMN.*', r'\(TABLE', r'\(SELECTsOME.*', r'\(REC.*', r'\(WHERE.*',r'\(COND.*',r'\(CONJ.*']
    for i in chunks:
        grammar_record = i
        chunk_parser = nltk.RegexpParser(grammar_record)
        chunk_results = chunk_parser.parse(new_tokens)
        #print(chunk_results)
        for m in test:
            pattern = re.compile(m)
            for j in chunk_results:
                #print(j)
                if pattern.match(str(j)) and m == '\(COLUMN.*':
                    query.append(j) 
                elif pattern.match(str(j)) and not m == '\(COND.*':
                    query.append(j)
                    break
                elif pattern.match(str(j)) and m == '\(COND.*':
                    query.append(j)
    return query            

# Main Parser for the system. This takes the individual chunks and parses them into 
# the individual parts of the Sql Query. This also does sub chunking for the query by taking 
# the sub chunks of a specific Variety (COND.*) and chunking them for various comparisons. 
# It does this by recreating a sent from the chunk and then chunking this new sentence. 

def parser(chunks):
    table = False
    default = 'Takata'
    query = []
    last_column = ""
    last_comparison = ""
    limit = 0
    for chunk in chunks:
        if chunk[0] == "TABLE":
            table = True

    for chunk in chunks:

        #print(chunk)
        #print(" ".join([x for x,_ in chunk]))
        if chunk._label == 'SELECTaLL' and table:
            #query.append("SELECT * from")
            query.append("SELECT *")
            query.append("FROM")
        elif chunk._label == 'SELECTaLL' and not table: 
            for chip, label in chunk:
                if re.compile(r'DT').match(label) and chip == "all":
                    #query.append("SELECT * from %s" %(default))
                    query.append("SELECT *")
                    query.append("FROM")
                    query.append("%s" %(default))
                if re.compile(r'DT').match(label) and chip == "the":
                    query.append("SELECT")
            #query.append("SELECT * from %s" %(default))
        elif chunk._label == 'SELECTsOME' and not table:
            query.append("SELECT *")
            query.append("FROM")
            query.append("%s" %(default))
            for chip, label in chunk:
                if re.compile(r'CD').match(label) and chip.isdigit():
                    limit = int(chip)
             
        elif chunk._label == 'TABLE': 
            for i in range(len(chunk)):
                if len(chunk[i]) == 2 and chunk[i][0] == 'from' and chunk[i][1] == 'IN':
                    query.append(chunk[i+1][0])

        elif re.compile(r'\(COLUMN.*').match(str(chunk)):
            for chip,label in chunk: 
                if re.compile(r'NNP|VBN').match(label) and chip not in query:
                    #print(chunk)
                    query.append(chip+",")
                    
        # get: where column (cond) value | and column (cond) value
        sent = " ".join([x for x,_ in chunk])
        new_tokens = nltk.pos_tag(word_tokenize(sent))
        chunk_a = [
            r"""CC: {<CC>+}""",
            r"""IS_NOT: {<VBZ>+<RB>+}""",
            r"""IS_NOT_EQUAL_TO: {<VBZ>+<RB>+<JJ>+<TO>+}""",
            r"""EQUALS: {<NNP>+<NNS>+<VBP|CD|NN.?>+}
                          }<NNP|VBP|CD>+{""",
            r"""ISEQUAL: {<VBZ>+<NN.?>+}
                          }<NN.?>+{""",
            r"""IS_EQUAL_TO: {<VBZ>+<JJ>+<TO>+}""",
            r"""IS_GL_THAN: {<VBZ>+<JJR>+<IN>+}""",
        ]

        for i in chunk_a:
            chunk_parser = nltk.RegexpParser(i)
            chunk_results = chunk_parser.parse(new_tokens)
            #print(chunk_results)
            for j in chunk_results:
                if chunk._label == 'COND1':
                    #print(chunk)
                    #print(j)
                    column = ""            
                    value = ""             
                    condition = ""
                    equal = ""
                    comparison = ""
                    if re.compile(r'\(CC.*').match(str(j)):
                        condition = str(chunk).split(" ")[1].split("/")[0]
                    if re.compile(r'\(IS_NOT.*').match(str(j)):
                        #print("IS_NOT")
                        comparison = "!="
                        a = " ".join(["%s/%s" %(i[0],i[1]) for i in j])
                        #last_column = column = str(chunk).split(a,1)[0].split(" ",1)[1].split("/",1)[0].strip()
                        last_column = column = str(chunk).split(a,1)[0].strip().split(" ")[-1].split("/",1)[0].strip()
                        value = str(chunk).split(a,1)[1].split("/",1)[0].strip()
                        if re.compile(r'greater|less|equal').match(str(value)) : continue
                    if  re.compile(r'\(IS_NOT_EQUAL_TO.*').match(str(j)):
                        #print("IS_NOT")
                        comparison = "!="
                        a = " ".join(["%s/%s" %(i[0],i[1]) for i in j])
                        #last_column = column = str(chunk).split(a,1)[0].split(" ",1)[1].split("/",1)[0].strip()
                        last_column = column = str(chunk).split(a,1)[0].strip().split(" ")[-1].split("/",1)[0].strip()
                        value = str(chunk).split(a,1)[1].split("/",1)[0].strip()
                    elif re.compile(r'\(EQUALS.*').match(str(j)) or\
                         re.compile(r'\(ISEQUAL.*').match(str(j)) or\
                         re.compile(r'\(IS_EQUAL_TO.*').match(str(j)):
                         #print("ISEQUAL")
                        comparison = "="
                        a = " ".join(["%s/%s" %(i[0],i[1]) for i in j])
                        last_column = column = str(chunk).split(a,1)[0].strip().split(" ")[-1].split("/",1)[0].strip()
                        #last_column = column = str(chunk).split(a,1)[0].strip().split(" ")[-1].split("/",1)[0].strip()
                        value = str(chunk).split(a,1)[1].split("/",1)[0].strip()
                    elif re.compile(r'\(IS_GL_THAN.*').match(str(j)):
                        #print("IS_GL_THAN")
                        if re.compile(r'.*greater.*|.*bigger.*|.*larger.*', re.IGNORECASE).match(str(j)):
                            comparison = ">"
                        elif re.compile(r'.*less.*|.*smaller.*', re.IGNORECASE).match(str(j)):
                            comparison = "<"

                        a = " ".join(["%s/%s" %(i[0],i[1]) for i in j])
                        last_column = column = str(chunk).split(a,1)[0].strip().split(" ")[-1].split("/",1)[0].strip()
                        #last_column = column = str(chunk).split(a,1)[0].strip().split(" ")[-1].split("/",1)[0].strip()
                        value = str(chunk).split(a,1)[1].split("/",1)[0].strip()

                    if value and not value.isdigit():
                        value = "'%s'" % (value)
                    if column and value and comparison and "WHERE" not in query and "FROM" not in query:                
                        for k in ["FROM", default, "WHERE", column, comparison, value]:
                            query.append(k)
                        #print (query)
                    elif column and value and comparison and "WHERE" not in query:                
                        for k in ["WHERE", column, comparison, value]:
                            query.append(k)
                        #print (query)
                    else:
                        for k in [condition, column, comparison, value]:
                            query.append(k)
                    #break
                        #print (query)
                elif chunk._label == 'CONDn' and last_column:
                    #print(sent)
                    condition =""
                    value = ""            
                    comparison = "="
                    for chip,label in chunk: 
                        if re.compile(r'CC').match(str(label)):
                            condition = chip
                        if re.compile(r'NN.?').match(str(label)):
                            value = chip
                    if value and not value.isdigit():
                        value = "'%s'" % (value)
                    if condition and value and not value in query: 
                        for item in [condition, last_column, comparison, value]:
                            query.append(item)
                        break
    if limit > 0:
        query.append("LIMIT %d"%(limit))
    query.append(";")
    #return query
    #return " ".join(query).replace("  ", " ")
    #print(query)
    return " ".join(filter(None, query)).replace(", FROM", " FROM")

def query(sent):
        chunks = chunker(sent)            
        sqlSent = parser(chunks)
        try:    
            return getDatafromDatabse(sqlSent) 
        except:
            raise Exception("Unable to process query: %s"%(sent))
            #print("Unable to process query: %s"%(sent))
            #print("please check the query and try again....")

if __name__ == "__main__":
    #print(" ".join(sys.argv[1:]))
    databaseCall = False
    if len(sys.argv) > 1 and sys.argv[1] == "--show-data":
        databaseCall = True
    
    if len(sys.argv) > 1  and sys.argv[1] == "-d":
        os. system("mysql --user='IRobot' -p'toor' --database='aidb' --execute=\"%s\""%("describe Takata;"))
        sys.exit()

    elif len(sys.argv) > 1  and sys.argv[1] == "-h":
        print("%s [-d | -h | --show-data] "%(sys.argv[0]))
        print(" -d          -- Describe table  ")
        print(" -h          -- Show this menu  ")
        print(" --show-data -- Modifier to show queries ")
        sys.exit()
    elif len(sys.argv) == 1 or sys.argv[1] == "--show-data" and len(sys.argv) == 2: 
        for i in range(len(sent_arr)):
            sent = sent_arr[i]
            chunks = chunker(sent)            
            sqlsent = parser(chunks)
            try:
                #print(query(sent))
                print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                print(sent)
                print(sqlsent)
                sys.stdout.flush()
                getDatafromDatabase(sqlsent) 
                if databaseCall:
                    os. system("mysql --user='IRobot' -p'toor' --database='aidb' --execute=\"%s\""%(sqlsent))
            except:
                #print("unexpected error: %s "%(sys.exc_info()[0]))
                print("unable to process query: %s"%(sent))
                print("please check the query and try again....")
            #print(getdatafromdatabse(sqlsent))
        
    else:   
        if databaseCall:
            sent = " ".join(sys.argv[2:])
        else:
            sent = " ".join(sys.argv[1:])
        chunks = chunker(sent)            
        sqlsent = parser(chunks) 
        try:
            #print(query(sent))
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            print(sent)
            print(sqlsent)
            sys.stdout.flush()
            getDatafromDatabase(sqlsent) 
            if databaseCall:
                os. system("mysql --user='IRobot' -p'toor' --database='aidb' --execute=\"%s\""%(sqlsent))
        except:
            #print("unexpected error: %s "%(sys.exc_info()[0]))
            print("unable to process query: %s"%(sent))
            print("please check the query and try again....")
        #print(getdatafromdatabse(sqlsent))
