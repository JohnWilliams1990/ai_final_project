#!/bin/bash

file='data.csv'
database='aidb';

mysql --database="$database" --execute="GRANT ALL ON $database.* TO 'IRobot'    @'localhost' IDENTIFIED BY 'toor';"

wget https://data.transportation.gov/api/views/8u28-hw9f/rows.csv?accessType=DOWNLOAD -O $file
sed -i 's|\ /\ |_|g; s/¹//g; s/²//g; s/³//g; s/⁴//g; s/⁵//g; s/⁶//g; s/\ /_/g;' data.csv 
sed -E 's,([0-9]{2})/([0-9]{2})/([0-9]{4}),\3/\1/\2,g' data.csv -i


mysql < Database

mysql --database="$database" --execute="load data local infile '$file' into table Takata fields terminated by ',' lines terminated by '\n' IGNORE 1 ROWS (Report_Date,Manufacturer,Recall_Campaign,Priority_Group,Driver_Passenger,Total_Air_Bags_Affected,Total_Air_Bags_Repaired,Scrapped,Exported,Stolen,Other,Net_Air_Bags_Affected,Net_Air_Bags_Remaining,Completion_Rate);"

mysql --database="$database" --execute="select * from Takata;" 2>/dev/null 
mysql --database="$database" --execute="SELECT COUNT(*) FROM Takata;" 2>/dev/null 
# mysql --database="$database" --execute="drop Table IF EXISTS Takata ;"
