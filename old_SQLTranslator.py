#!/usr/bin/python3
# https://www.youtube.com/watch?v=X2vAabgKiuM
# https://www.youtube.com/watch?v=imPpT2Qo2sk&list=PLQVvvaa0QuDf2JswnfiGkliBInZnIC4HL&index=5 
import os 
import re 
import tkinter
import nltk
import inspect
import nltk.corpus
from nltk.tokenize import word_tokenize
from nltk import pos_tag
from nltk import RegexpParser
from nltk.corpus import stopwords
import mysql.connector 

def getDatafromDatabse(command):
    # connect to database
    connection = mysql.connector.connect(
        host = 'localhost',
        user = 'IRobot',
        password = 'toor',
        database = 'aidb'
    )

    myCursor = connection.cursor()
    myCursor.execute(command)

    data = myCursor.fetchall()
    connection.close()
    return data


key = """
CC	coordinating conjunction
CD	cardinal digit
DT	determiner
EX	existential there (like: "there is" ... think of it like "there exists")
FW	foreign word
IN	preposition/subordinating conjunction
JJ	adjective	'big'
JJR	adjective, comparative	'bigger'
JJS	adjective, superlative	'biggest'
LS	list marker	1)
MD	modal	could, will
NN	noun, singular 'desk'
NNS	noun plural	'desks'
NNP	proper noun, singular	'Harrison'
NNPS	proper noun, plural	'Americans'
PDT	predeterminer	'all the kids'
POS	possessive ending	parent\'s
PRP	personal pronoun	I, he, she
PRP$	possessive pronoun	my, his, hers
RB	adverb	very, silently,
RBR	adverb, comparative	better
RBS	adverb, superlative	best
RP	particle	give up
TO	to	go 'to' the store.
UH	interjection	errrrrrrrm
VB	verb, base form	take
VBD	verb, past tense	took
VBG	verb, gerund/present participle	taking
VBN	verb, past participle	taken
VBP	verb, sing. present, non-3d	take
VBZ	verb, 3rd person sing. present	takes
WDT	wh-determiner	which
VBPWP	wh-pronoun	who, what
WP$	possessive wh-pronoun	whose
WRB	wh-abverb	where, when
"""
print(key)

sent = 'give me all records' #
#sent = 'give me 100 records where x is greater than 100'
#sent = 'give me 100 records where x is greater than 100'
#sent = 'give me 100 records where x is less than 100'
#sent = 'get all of the records' #
sent = 'give me all records where the Manufacturer is BMW' #
#sent = 'give me all records where the Manufacturer is BMW and the Manufacturer is Ferrari' # 
#sent = 'give me all records where the Recall_Campaign is greater than 100'
#sent = 'give me all records where the Recall_Campaign is equal to adasd12123'
#sent = 'give me all records where the Recall_Campaign equals adasd12123' #
#sent = 'give me all records where the Manufacturer is not BMW' #
#sent = 'give me all records where the Manufacturer isn\'t BMW' #

sent = 'give me all records where the Manufacturer is BMW or Ferrari' #
#sent = 'give me all records where the Manufacturer is BMW or Ferrari and Ford' #

#sent = 'give me all records where the Recall_Campaign is less than 100 or greater than 10 '
#sent = 'give me all records where the Recall_Campaign is less than 100 or the Manufacturer is BMW '
#sent = 'give me all records where the Recall_Campaign is less than 100 or greater than 10 '
#sent = 'give me all records from Takata where the Recall_Campaign is less than 100 or greater than 10 '

#sent = 'give me all records where the Manufacturer is not equal to BMW' #
sent_arr = [
#  'get all of the records',
#  'give me 100 records where x is greater than 100',
#  'give me 100 records where x is less than 100',
#  'give me all records where the Manufacturer is BMW',
#  'give me all records where the Manufacturer is BMW and the Manufacturer is Ferrari',
#  'give me all records where the Recall_Campaign is greater than 100',
#  'give me all records where the Recall_Campaign is equal to adasd12123',
#  'give me all records where the Recall_Campaign equals adasd12123',
#  'give me all records where the Manufacturer is not BMW',
#  'give me all records where the Manufacturer isn\'t BMW',
#  'give me all records where the Manufacturer is BMW or Ferrari',
  'give me all records where the Manufacturer is BMW or Ferrari and Ford' 
]


def chunker(sent):
    #r"""SELECT_ALL: {<VB|VBP>+<PRP|PRP$>?<DT>}""",
    #r"""SELECT_ALL: {<VB|VBP>+<DT>+}""",
    
    new_tokens = nltk.pos_tag(word_tokenize(sent))
    #print (new_tokens)
    query = []
    # Iterate through chunks using the chunker to classify different parts of speach that could be classified as SQL
    # For each of these use a regex on the label of the chunk to see if we had a hit. 
    # Append this to a list and use this t create the sql query based on conditions
    chunks = [
        r"""SELECTaLL: {<VB|VBP>+<PRP.?>*<DT>+}""",
        r"""SELECTsOME: {<VB|VBP>+<PRP|PRP$>?<CD>}""",
        r"""REC: {<.*>+<NN|NNS>+$}""",
        r"""TABLE:{<NNS>+<IN>+<NNP>+}""",
        #r"""COND1: {<WRB|CC>*<DT>*<NN.?>+<VB.?>+<NN.?|JJ.?>*<IN|TO>*<NNP|CD|VB.?>+}
        r"""COND1: {<WRB|CC>*<DT>*<NN.?>+<VB.?|NN.?>+<NN.?|JJ.?|RB>*<IN|TO>*<NNP|CD|VB.?>+}
              }<WRB>+<DT>+{""" ,
        #r"""COND1: {<CC>*<DT>*<NN.?>+<VB.?|NN.?|JJ.?>+<IN>*<NNP|CD|VB.?>+} """ , #}<DT>+{
        #r"""CONDn: {<CC>+<NN.?>+}""" ,
        r"""CONDn: {<CC>+<RB>*<NNP|CD|VBP>+}""" ,
        r"""CONDN: {<CC>+<RB>*<VB.?|NN.?|JJ.?>+<IN>*<NNP|CD|VBP>+}""" ,
        r"""COND@: {<CC>+<RB>*<DT>*<NN.?>+<VB.?|JJ.?>*<NNP|CD|VB.?>+}""" ,
        #r"""CONJ: {<CC>+<DT>*<NN.?>+<VB.?>*<NNP>+}"""
#('where', 'WRB'), ('the', 'DT'), ('Manufacturer', 'NNP'), ('is', 'VBZ'), ('BMW', 'NNP')
    ]
    test = [r'\(SELECTaLL.*', r'\(TABLE', r'\(SELECTsOME.*', r'\(REC.*', r'\(WHERE.*',r'\(COND.*',r'\(CONJ.*']
    for i in chunks:
        grammar_record = i
        chunk_parser = nltk.RegexpParser(grammar_record)
        chunk_results = chunk_parser.parse(new_tokens)
        #print(chunk_results)
        for m in test:
            pattern = re.compile(m)
            for j in chunk_results:
                #print(j)
                #print(len(j))
                #if len(j) > 2 and pattern.match(str(j)):
                if pattern.match(str(j)) and not m == '\(COND.*':
                    #print(j)
                    query.append(j)
                    break
                elif pattern.match(str(j)) and m == '\(COND.*':
                    #print(j)
                    query.append(j)
                    #break
                    #break
    return query            




def parser(chunks):
    table = False
    default = 'Takata'
    query = []
    last_column = ""
    last_comparison = ""
    for chunk in chunks:
        if chunk[0] == "TABLE":
            table = True

    for chunk in chunks:

        #print(" ".join([x for x,_ in chunk]))
        #print(type(chunk))
        #print(chunk.__dict__)
        if chunk._label == 'SELECTaLL' and table: 
            query.append("SELECT * from")
        elif chunk._label == 'SELECTaLL' and not table: 
            query.append("SELECT * from %s" %(default))
        elif chunk._label == 'TABLE': 
            for i in range(len(chunk)):
                if len(chunk[i]) == 2 and chunk[i][0] == 'from' and chunk[i][1] == 'IN':
                    query.append(chunk[i+1][0])
                    #print(chunk[i])

        # get: where column (cond) value | and column (cond) value
        elif chunk._label == 'COND1':
            column = ""
            value = ""
            condition = ""
            equal = ""
            comparison = ""

            sent = " ".join([x for x,_ in chunk])
            new_tokens = nltk.pos_tag(word_tokenize(sent))
            #(S Recall_Campaign/NNP is/VBZ equal/JJ to/TO adasd12123/VB)


#########################
            chunk_a = [
                r"""CC: {<CC>+}""",
                r"""IS_NOT: {<VBZ>+<RB>+|}""",
                r"""IS_NOT_EQUAL_TO: {<VBZ>+<RB>+<JJ>+<TO>+}""",
                r"""EQUALS: {<NNP>+<NNS>+<VBP|CD|NN.?>+}
                              }<NNP|VBP|CD>+{""",
                r"""ISEQUAL: {<VBZ>+<NN.?>+}
                              }<NN.?>+{""",
                r"""IS_EQUAL_TO: {<VBZ>+<JJ>+<TO>+}""",
                r"""IS_GL_THAN: {<VBZ>+<JJR>+<IN>+}""",
            ]

            for i in chunk_a:
                #grammar_record = i
                chunk_parser = nltk.RegexpParser(i)
                chunk_results = chunk_parser.parse(new_tokens)
                #print(chunk_results)
                for j in chunk_results:

#########################
                    if re.compile(r'\(CC.*').match(str(j)):
                        condition = str(chunk).split(" ")[1].split("/")[0]
                       # print(j)


                    if re.compile(r'\(IS_NOT.*').match(str(j)) or\
                       re.compile(r'\(IS_NOT_EQUAL_TO.*').match(str(j)):
                        #print("IS_NOT")
                        #print(j)
                        comparison = " != "
                        a = " ".join(["%s/%s" %(i[0],i[1]) for i in j])
                        #last_column = column = str(chunk).split(a,1)[0].split(" ",1)[1].split("/",1)[0].strip()
                        last_column = column = str(chunk).split(a,1)[0].strip().split(" ")[-1].split("/",1)[0].strip()
                        value = str(chunk).split(a,1)[1].split("/",1)[0].strip()

                        break
                    elif re.compile(r'\(EQUALS.*').match(str(j)) or\
                         re.compile(r'\(ISEQUAL.*').match(str(j)) or\
                         re.compile(r'\(IS_EQUAL_TO.*').match(str(j)):
                         #print("ISEQUAL")

                        comparison = " = "
                        a = " ".join(["%s/%s" %(i[0],i[1]) for i in j])
                        #print(a)
                        #print(str(chunk).split(a,1))
                        #print(str(chunk).split(a,1)[0].split(" "))
                        last_column = column = str(chunk).split(a,1)[0].strip().split(" ")[-1].split("/",1)[0].strip()
                        #last_column = column = str(chunk).split(a,1)[0].strip().split(" ")[-1].split("/",1)[0].strip()
                        value = str(chunk).split(a,1)[1].split("/",1)[0].strip()
                         
                        #equal = " = "
                        break
                    elif re.compile(r'\(IS_GL_THAN.*').match(str(j)):
                        #print("IS_GL_THAN")
                        if re.compile(r'.*greater.*|.*bigger.*|.*larger.*', re.IGNORECASE).match(str(j)):
                            comparison = " > "
                        elif re.compile(r'.*less.*|.*smaller.*', re.IGNORECASE).match(str(j)):
                            comparison = " < "
                        #print(comparison)
                        #print(j)

#['(COND1 where/WRB x/NN is/VBZ less/JJR than/IN 100/CD)']
#(IS_GL_THAN is/VBZ less/JJR than/IN)
#[('is', 'VBZ'), ('less', 'JJR'), ('than', 'IN')]

                        #print(str(chunk).split(str(j[0]),1))
                        a = " ".join(["%s/%s" %(i[0],i[1]) for i in j])
                        #print(str(chunk).split(a,1))
                        #column = str(chunk).split(a,1)[0].split(" ",1)[1].split("/",1)[0].strip()
                        last_column = column = str(chunk).split(a,1)[0].strip().split(" ")[-1].split("/",1)[0].strip()
                        #last_column = column = str(chunk).split(a,1)[0].strip().split(" ")[-1].split("/",1)[0].strip()
                        value = str(chunk).split(a,1)[1].split("/",1)[0].strip()
                        #if not value.isdigit():
                        #    value = "'%s'" % (value)
                        
                        #print(a)
                        #print(column)
                        #print(str(chunk).split(a,1)[0])
                        #print(str(chunk).split(a,1)[0].strip().split(" "))
                        #print("")
                        #print("")
                        ##print(str(chunk).split(a,1)[0].split(" ",1)[-1].split("/",1)[0].strip())
                        ##print(str(chunk).split(a,1)[0].split(" ",1)[1].split("/",1)[0].strip())
                        ##print(str(chunk).split(a,1)[0].split(" ",1)[0].split("/",1)[0].strip())
                        #print(column)
                        #print(value)
                        #print("@@@@@@@@@@@@@@@@@@@@@@@@")
                        ##for k in [ column, comparison, value]:
                        ##    query.append(j)
                        ##print (query)

                        break
                #if not (comparison and equal): 
            if value and not value.isdigit():
                value = "'%s'" % (value)
            if column and value and comparison and "WHERE" not in query:                
                for k in ["WHERE", column, comparison, value]:
                    query.append(k)
                #print (query)
            else:
                for k in [condition, column, comparison, value]:
                    query.append(k)
                #print (query)


#        # get trailing: and x and y and z
#        elif chunk._label == 'CONDn' and last_column: #and last_comparison: 
#            column = last_column
#            comparison = last_comparison
#            value = ""
#            condition = ""
#            for item in chunk:
#                if item[1] == 'CC':
#                    condition = item[0]
#                    print(item[0] )
#                    print(item[0] )
#                elif re.compile(r'NN.*').match(item[1]):
#                    value = "'%s'" % (item[0])
#                    print(item[0] )
#                    print(item[0] )
#                    print(item[0] )
#                    print(last_comparison)
#                    print(last_comparison)
#                    print(last_comparison)
#                    print("value" )
#                if column and value and comparison and "WHERE" not in query:
#                     for j in ["WHERE", condition, column, comparison, value]:
#                         query.append(j)
#                     print (query)
#                elif column and value and condition:
#                     for j in [ condition, column, comparison, value]:
#                         query.append(j)
#                     print (query)
#            print(chunk)
#        elif chunk._label == 'CONDN':
#            print(chunk)
#            #pass
    query.append(";")
    return " ".join(query).replace("  ", " ")


for i in range(len(sent_arr)):
    sent = sent_arr[i]
    chunks = chunker(sent)            
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print(len(chunks))
    print(sent)
    print(parser(chunks))
 

#for i in chunks:
#    print(i)




#def parser(chunks):
#    table = False
#    default = 'Takata'
#    query = []
#    last_column = ""
#    last_comparison = ""
#    for chunk in chunks:
#        if chunk[0] == "TABLE":
#            table = True
#
#    for chunk in chunks:
#        print(type(chunk))
#        print(chunk.__dict__)
#        if chunk._label == 'SELECTaLL' and table: 
#            query.append("SELECT * from")
#        elif chunk._label == 'SELECTaLL' and not table: 
#            query.append("SELECT * from %s" %(default))
#        elif chunk._label == 'TABLE': 
#            for i in range(len(chunk)):
#                if len(chunk[i]) == 2 and chunk[i][0] == 'from' and chunk[i][1] == 'IN':
#                    query.append(chunk[i+1][0])
#                    print(chunk[i])
#        # get: where column (cond) value | and column (cond) value
#        elif chunk._label == 'COND1':
#            print(" ".join([x for x,_ in chunk]))
#            column = ""
#            comparison = ""
#            value = ""
#            condition = ""
#            print (chunk)
#            for item in chunk:
#                if not condition and item[1] == 'CC':
#                    print("Conditional" )
#                    condition = item[0]
#                elif not column and re.compile(r'NN.*').match(item[1]) and comparison == "":
#                    print("column" )
#                    print(item[0] )
#                    column = item[0]
#                    last_column = item[0]
#                elif not value and re.compile(r'NN.*|CD|VBP|VB').match(item[1]) and comparison != "":
#                    print("value" )
#                    if item[1] == "CD":
#                        value = "%d" % (item[0])
#                    else:
#                        value = "'%s'" % (item[0])
#                #elif not comparison and re.compile(r'VB.?|NNS').match(item[1]):
#                elif comparison and re.compile(r'VB.?|NNS|JJ').match(item[1]):
#                    print("comparison" )
#                    print(item[0] )
#                    if re.compile(r'is|equal[s]*').match(item[0]):
#                        comparison = " = "
#                        last_comparison = " = "
#                    if re.compile(r'greater').match(item[0]):
#                        comparison = " > "
#                        last_comparison = " > "
#                    if re.compile(r'less').match(item[0]):
#                        comparison = " < "
#                        last_comparison = " < "
#
#            if column and value and comparison and "WHERE" not in query:
#                 # condition should be absent here
#                 #for j in ["WHERE", condition, column, comparison, value]:
#                 for j in ["WHERE", column, comparison, value]:
#                     query.append(j)
#                 print (query)
#            elif column and value and comparison:
#                 for j in [ condition, column, comparison, value]:
#                     query.append(j)
#                 print (query)
#
#        # get trailing: and x and y and z
#        elif chunk._label == 'CONDn' and last_column: #and last_comparison: 
#            column = last_column
#            comparison = last_comparison
#            value = ""
#            condition = ""
#            for item in chunk:
#                if item[1] == 'CC':
#                    condition = item[0]
#                    print(item[0] )
#                    print(item[0] )
#                elif re.compile(r'NN.*').match(item[1]):
#                    value = "'%s'" % (item[0])
#                    print(item[0] )
#                    print(item[0] )
#                    print(item[0] )
#                    print(last_comparison)
#                    print(last_comparison)
#                    print(last_comparison)
#                    print("value" )
#                if column and value and comparison and "WHERE" not in query:
#                     for j in ["WHERE", condition, column, comparison, value]:
#                         query.append(j)
#                     print (query)
#                elif column and value and condition:
#                     for j in [ condition, column, comparison, value]:
#                         query.append(j)
#                     print (query)
#            print(chunk)
#        elif chunk._label == 'CONDN':
#            print(chunk)
#            #pass
#    query.append(";")
#    return " ".join(query).replace("  ", " ")















#### #chunk_parser = nltk.RegexpParser(grammar_select)
#### #chunk_parser = nltk.RegexpParser(grammar_select_range)
#### #chunk_parser = nltk.RegexpParser(grammar_record)
#### #chunk_results = chunk_parser.parse(new_tokens)
#### 
#### #chunk_results.draw()
#### 
#### 
#### print(sent)
#### 
#### command = []
#### verbs = ['get', 'give']
#### numbers = ['all','everyone','every']
#### nouns_for_record = ['record', 'records']
#### adverbs_for_where = ['where', 'when']
#### nouns_for_feild = ['record', 'records']
#### nouns_for_equiv = ['equals', 'equal', 'is' ]
#### 
#### 
#### 
#### #for i in chunk_results:
#### #    print(i)
#### #    if i[1] in ['VB','VBP']  and i[0] in verbs:
#### #        print("verb")
#### #        command.append('select')
#### #    elif i[1] in ['PRP','PRP$']:
#### #        print("pronoun")
#### #        pass
#### #    elif i[1] == 'DT' and i[0] in numbers:
#### #        print("number")
#### #        command.append('*')
#### #    elif i[1] in ['NN', 'NNS'] and i[0] in nouns_for_record:
#### #        print("record")
#### #        command.append('from Takata')
#### #
#### 
#### # print commands that will be recognised:
#### command_str = " ".join(command)
#### print(command_str )
#### #print( command.join(" ") )
####    
####     
#### 
#### 
#### # #for i in getDatafromDatabse("select * from Takata"):
#### # for i in getDatafromDatabse(command_str):
#### #     print(i)
#### 
#### # for i in chunck_results:
#### #     print(i)
#### 



        #r"""WHERE: {<.*>+<NN|NNS>+<WRB>+}""", --> NOT VERY USEFULL
        #r"""COND: {<WRB>+<NN.?>+<VB.?>*<JJ.?>+<IN>*<CD.?>+}""" 
        #r"""COND: {<WRB>+<DT>*<NN.?>+<VB.?>*<NNP>+}""" 
        #r"""COND: {<NN.?>+<VB.?>*<NNP>+}""" ,
        #make cond have NNP+ (CC* NNP*)*
        #r"""COND1: {<NN.?>+<VB.?>*<NNP>+}""" ,
        #r"""COND1: {<NN.?>+<VB.?|NN.?|JJ.?>*<IN>*<NNP|CD|VBP>+}""" ,
        #r"""COND1: {<CC>*<DT>*<NN.?>+<VB.?|NN.?|JJ.?>+<IN>*<NNP|CD|VBP>+}""",
#[('give', 'VB'), ('me', 'PRP'), ('all', 'DT'), ('records', 'NNS'), ('where', 'WRB'), ('the', 'DT'), ('Recall_Campaign', 'NNP'), ('is', 'VBZ'), ('equal', 'JJ'), ('to', 'TO'), ('adasd12123', 'VB')]

        #r"""COND1: {<WRB>*<CC>*<DT>*<NN.?>+<VB.?|NN.?|JJ.?>+<IN>*<NNP|CD|VB.?>+}

        #r"""COND1: {<WRB|CC>*<DT>*<NN.?>+<VB.?>+<NN.?|JJ.?>+<IN|TO>*<NNP|CD|VB.?>+}
#[('give', 'VB'), ('me', 'PRP'), ('all', 'DT'), ('records', 'NNS'), ('where', 'WRB'), ('the', 'DT'), ('Recall_Campaign', 'NNP'), ('equals', 'NNS'), ('adasd12123', 'VBP')

